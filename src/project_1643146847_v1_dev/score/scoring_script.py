import click
import mlflow
from project_1643146847_v1_dev.utils import spark_udf
from pyspark.sql import SparkSession, DataFrame
from pyspark.sql import functions as F

mlflow.set_registry_uri('http://it-bigdata-preprod.megafon.ru:8088')
mlflow.set_tracking_uri('http://it-bigdata-preprod.megafon.ru:8088')


def readData(spark, inputTableName, date):
    pass


def preprocessData(temp_df):
    return temp_df


def write_results(spark, result_df: DataFrame, output_table_name: str, snap_date: str):
    pass


def scoring(spark, model_name, stage, input_table_name, output_table_name, snap_date='2021-09-07'):
    input_df = readData(spark, input_table_name, snap_date)

    udf = spark_udf(spark, "models:/{}/{}".format(model_name, stage), 'predict_proba',
                    result_type='array<double>')
    scoredDf: DataFrame = input_df.withColumn('score', udf())

    result_df = scoredDf.select('sk_subs_id', F.element_at('score', 1).alias('score'),
                                F.lit(snap_date).alias('score_date'))
    write_results(spark, result_df, output_table_name, snap_date)


@click.command()
@click.option('-m', '--model-name', default='project_1643146847_v1_dev',
              help='Name of model in mlflow model registry')
@click.option('-s', '--stage', default='Production', help='Name of stage to use in scoring')
@click.option('-d', '--snap-date', required=True, help='Snapshot date of data in input table')
def main(model_name, stage, snap_date):
    spark = SparkSession.builder.enableHiveSupport().getOrCreate()
    # TODO change output table
    scoring(spark, model_name, stage, "dm.a_udm_weekly", "", snap_date)
    spark.stop()


if __name__ == '__main__':
    main()
